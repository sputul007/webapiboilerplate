﻿using Microsoft.Extensions.DependencyInjection;
using WebApiBoilerplate.Domain.Services;
using WebApiBoilerplate.Services;

namespace WebApiBoilerplate.Extensions
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddServiceDependency(this IServiceCollection services)
        {
            services.AddScoped<IWeatherForecastService, WeatherForecastService>();
            return services;
        }
    }
}
