﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiBoilerplate.Domain.Communication
{
    public class OkResponse<T> : BaseResponse
    {
        public T Result { get; }

        public int TotalCount { get; set; } = 0;

        public OkResponse(T result)
            : base(200)
        {
            Result = result;
        }

        public OkResponse(T result, int totalCount)
           : base(200)
        {
            Result = result;

            TotalCount = totalCount;
        }
    }
}
