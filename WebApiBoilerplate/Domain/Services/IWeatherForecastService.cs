﻿using System.Collections.Generic;
using WebApiBoilerplate.Domain.Models;

namespace WebApiBoilerplate.Domain.Services
{
    public interface IWeatherForecastService
    {
        IEnumerable<WeatherForecast> GetData();
    }
}
