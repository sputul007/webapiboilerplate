﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiBoilerplate.Domain.Models;
using Serilog;
using Microsoft.Extensions.DependencyInjection;
using WebApiBoilerplate.Domain.Services;

namespace WebApiBoilerplate.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : Controller
    {
        private readonly IServiceProvider _service;
        public WeatherForecastController(IServiceProvider service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            Log.Information("Executing Sample API at controller level");
            var weatherForecastService = _service.GetRequiredService<IWeatherForecastService>();
            return weatherForecastService.GetData();
        }
    }
}
