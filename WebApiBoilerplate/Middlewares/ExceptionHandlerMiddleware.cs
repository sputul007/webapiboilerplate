﻿using System;
using System.Net;
using System.Threading.Tasks;
using WebApiBoilerplate.Domain.Communication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace WebApiBoilerplate.Middlewares
{
    /// <summary>  
    /// Different types of exceptions.  
    /// </summary>  
    public enum AllExceptions
    {
        NullReferenceException = 1,
        FileNotFoundException = 2,
        OverflowException = 3,
        OutOfMemoryException = 4,
        InvalidCastException = 5,
        ObjectDisposedException = 6,
        UnauthorizedAccessException = 7,
        NotImplementedException = 8,
        NotSupportedException = 9,
        InvalidOperationException = 10,
        TimeoutException = 11,
        ArgumentException = 12,
        FormatException = 13,
        StackOverflowException = 14,
        SqlException = 15,
        IndexOutOfRangeException = 16,
        IOException = 17
    }

    /// <summary>
    /// Central error/exception handler Middleware
    /// </summary>
    public class ExceptionHandlerMiddleware
    {
        private const string JsonContentType = "application/json";
        private readonly RequestDelegate request;
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlerMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next.</param>
        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.request = next;
        }
        /// <summary>
        /// Invokes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public Task Invoke(HttpContext context) => this.InvokeAsync(context);
        async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await this.request(context);
            }
            catch (Exception exception)
            {
                var httpStatusCode = ConfigurateExceptionTypes(exception);
                // set http status code and content type
                context.Response.StatusCode = httpStatusCode;
                context.Response.ContentType = JsonContentType;
                // writes / returns error model to the response
                await context.Response.WriteAsync(
                    JsonConvert.SerializeObject(new ExceptionResponse()
                    {
                        StatusCode = context.Response.StatusCode,
                        Message = exception.Message
                    }));
                // context.Response.Headers.Clear();

            }
        }
        /// <summary>
        /// Configurates/maps exception to the proper HTTP error Type
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns></returns>
        private static int ConfigurateExceptionTypes(Exception exception)
        {
            AllExceptions tryParseResult;
            if (Enum.TryParse<AllExceptions>(exception.GetType().Name.ToString(), out tryParseResult))
            {
                switch (tryParseResult)
                {
                    case AllExceptions.NullReferenceException:
                        return (int)HttpStatusCode.LengthRequired;

                    case AllExceptions.FileNotFoundException:
                        return (int)HttpStatusCode.NotFound;

                    case AllExceptions.OverflowException:
                        return (int)HttpStatusCode.RequestedRangeNotSatisfiable;

                    case AllExceptions.OutOfMemoryException:
                        return (int)HttpStatusCode.ExpectationFailed;

                    case AllExceptions.InvalidCastException:
                        return (int)HttpStatusCode.PreconditionFailed;

                    case AllExceptions.ObjectDisposedException:
                        return (int)HttpStatusCode.Gone;

                    case AllExceptions.UnauthorizedAccessException:
                        return (int)HttpStatusCode.Unauthorized;

                    case AllExceptions.NotImplementedException:
                        return (int)HttpStatusCode.NotImplemented;

                    case AllExceptions.NotSupportedException:
                        return (int)HttpStatusCode.NotAcceptable;

                    case AllExceptions.InvalidOperationException:
                        return (int)HttpStatusCode.MethodNotAllowed;

                    case AllExceptions.TimeoutException:
                        return (int)HttpStatusCode.RequestTimeout;

                    case AllExceptions.ArgumentException:
                        return (int)HttpStatusCode.BadRequest;

                    case AllExceptions.StackOverflowException:
                        return (int)HttpStatusCode.RequestedRangeNotSatisfiable;

                    case AllExceptions.FormatException:
                        return (int)HttpStatusCode.UnsupportedMediaType;

                    case AllExceptions.IOException:
                        return (int)HttpStatusCode.NotFound;

                    case AllExceptions.IndexOutOfRangeException:
                        return (int)HttpStatusCode.ExpectationFailed;

                    default:
                        return (int)HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                return (int)HttpStatusCode.InternalServerError;
            }
        }
    }
}
