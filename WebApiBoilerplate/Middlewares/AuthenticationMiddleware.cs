﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Text.Json;
using WebApiBoilerplate.Domain.Communication;

namespace WebApiBoilerplate.Middlewares
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private const string JsonContentType = "application/json";
        private const string ApiKeyHeaderName = "Authorization";
        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Headers.TryGetValue(ApiKeyHeaderName, out var potentialApiKey))
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Response.ContentType = JsonContentType;
                await context.Response.WriteAsync(JsonSerializer.Serialize(new ExceptionResponse { 
                    StatusCode = context.Response.StatusCode,
                    Message = new UnauthorizedAccessException().Message
                }));
                return;
            }

            var apiKey = "BbsOm4X1BA";

            if (!apiKey.Equals(potentialApiKey))
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Response.ContentType = JsonContentType;
                await context.Response.WriteAsync(JsonSerializer.Serialize(new ExceptionResponse
                {
                    StatusCode = context.Response.StatusCode,
                    Message = new UnauthorizedAccessException().Message
                }));
                return;
            }
            await _next(context);
        }
    }
}
